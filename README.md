# Project Roadmap Cho Lập Trình Viên

## Giới Thiệu

Chào mừng bạn đến với dự án Roadmap cho Lập Trình Viên! Dự án này được tạo ra với mục tiêu hỗ trợ và hướng dẫn lập trình viên trong việc xây dựng và phát triển kỹ năng của họ.

## Cài đặt chạy project
  1. clone project
  2. cài node
  3. trong root project run lệnh `npm i`
  4. để chạy run lệnh `npm run dev`

## Liên hệ

Nếu bạn cảm thấy cần sự hỗ trợ hoặc muốn chia sẻ thêm với cộng đồng, đừng ngần ngại liên hệ với chúng tôi qua:

- Email: [huyxvd@gmail.com](mailto:huyxvd@gmail.com)
- Facebook: [facebook.com/huyxvd/](https://www.facebook.com/huyxvd/)
- LinkedIn: [linkedin.com/in/huyvd/](https://www.linkedin.com/in/huyvd/)
- Số Điện Thoại: 035 222 1769
- Zalo: 035 222 1769
